var reqURL = "telemetry.json";
var req = new XMLHttpRequest();

function get_telemetry(){
    req.open('GET', reqURL);
    req.responseType = 'json';
    req.send();
    req.onload = function(){
        json = req.response;
	vals = json['vals'];
	bars = json['bars'];
	Object.keys(vals).forEach(function(key){
	    update(key, vals[key], bars[key])
	});
    }
}

function update(key, val, bar){
    if (val[2] == undefined){
	update_value(key, val, bar);
    } else if (val[3] == undefined){
	update_xyz(key, val, bar);
    }else{
	update_flfrrlrr(key, val, bar);
    }
}

function update_xyz(key, val, bar){
    if (bar == undefined){
	var bar = [1,1,1];
    }
    update_value(key + 'X', val[0], bar[0]);
    update_value(key + 'Y', val[1], bar[1]);
    update_value(key + 'Z', val[2], bar[2]);
}

function update_flfrrlrr(key, val, bar){
    if (bar == undefined){
	var bar = [1,1,1,1];
    }
    update_value(key + 'FL', val[0], bar[0]);
    update_value(key + 'FR', val[1], bar[1]);
    update_value(key + 'RL', val[2], bar[2]);
    update_value(key + 'RR', val[3], bar[3]);
}

function update_value(key, val, bar){
    let textelem = document.getElementById(key + 'Text');
    if (textelem != null){
	textelem.textContent = val;
    }

    if (bar != undefined){
	if (bar < 0){
	    console.log("UNDERFLOW: " + key + ": " + val + "(" + bar + ")");
	    return;
	}
	let barelem = document.getElementById(key + 'Bar');
	if (barelem != null){
	    barelem.setAttribute("width", bar);
	}
    }
}

setInterval(get_telemetry, 100);
get_telemetry();
