# これは何?
Forza Horizon 4の「UDPレース遠隔測定データ出力」機能で送信されたテレメトリデータを
受信して、Webインタフェースにを提供するサーバです。

![Webインタフェース例](websample.jpg)

# 必要なもの
- てきとうなPC
- ruby 2.5以降

# つかいかた
- rubyインストール済みでUDPポート3939を空けてあるてきとうなPCにfh4telemetry.rbを展開して、実行
- FH4の「画面表示とゲームプレイ」から、
  - 「データ出力」を「オン」に
  - 「データ出力IPアドレス」を「fh4parser.rbを実行しているPCのIPアドレス」に
  - 「データ送信IPポート」を「3939」に
  - Webブラウザで http://PCのIPアドレス:3939 にアクセス

- あとはてきとうに走るだけ!
- FH4を実行しているPCには送信できない模様。

# 参考
https://forums.forzamotorsport.net/turn10_postsm926839_Forza-Motorsport-7--Data-Out--feature-details.aspx
