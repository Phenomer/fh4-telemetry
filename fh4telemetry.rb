#!/usr/bin/ruby
#-*- coding: utf-8 -*-

# https://forums.forzamotorsport.net/turn10_postsm926839_Forza-Motorsport-7--Data-Out--feature-details.aspx

require 'socket'
require 'stringio'
require 'json'
require 'webrick'

class FH4TelemetryServer
  def initialize(host: '0.0.0.0', port: 3939)
    s = UDPSocket.new
    s.bind(host, port)
    loop do
      res = StringIO.new(s.recv(324))
      yield parse(res)
    end
  end
  
  def parse(input)
    vals = {
      isRaceOn:  input.read(4).unpack('l')[0],
      timestampMS: input.read(4).unpack('L')[0],
      engineMaxRpm: input.read(4).unpack('e')[0],
      engineIdleRpm: input.read(4).unpack('e')[0],
      currentEngineRpm: input.read(4).unpack('e')[0],
      
      acceleration: input.read(12).unpack('eee'),
      velocity: input.read(12).unpack('eee'),
      angularVelocity: input.read(12).unpack('eee'),
      
      yaw: input.read(4).unpack('e')[0],
      pitch: input.read(4).unpack('e')[0],
      roll: input.read(4).unpack('e')[0],
      
      normalizedSuspensionTravel: input.read(16).unpack('eeee'),
      tireSlipRatio: input.read(16).unpack('eeee'),
      wheelRotationSpeed: input.read(16).unpack('eeee'),
      wheelOnRumbleStrip: input.read(16).unpack('eeee'),
      wheelInPuddleDepth: input.read(16).unpack('llll'),
      surfaceRumble: input.read(16).unpack('eeee'),
      tireSlipAngle: input.read(16).unpack('eeee'),
      tireCombinedSlip: input.read(16).unpack('eeee'),
      
      suspensionTravelMeters: input.read(16).unpack('eeee'),
      
      carOrdinal: input.read(4).unpack('l')[0],
      carClass: input.read(4).unpack('l')[0],
      carPerformanceIndex: input.read(4).unpack('l')[0],
      drivetrainType: input.read(4).unpack('l')[0],
      numCylinders: input.read(4).unpack('l')[0],
      
      unknown1: input.read(12).unpack('lll'),
      position: input.read(12).unpack('eee'),
      
      speed: input.read(4).unpack('e')[0],
      power: input.read(4).unpack('e')[0],
      torque: input.read(4).unpack('e')[0],
      
      tireTemp: input.read(16).unpack('eeee'),
      
      boost: input.read(4).unpack('e')[0],
      fuel: input.read(4).unpack('e')[0],
      
      distanceTraveled: input.read(4).unpack('e')[0],
      bestLap: input.read(4).unpack('e')[0],
      lastLap: input.read(4).unpack('e')[0],
      currentLap: input.read(4).unpack('e')[0],
      currentRaceTime: input.read(4).unpack('e')[0],
      
      lapNumber: input.read(2).unpack('S')[0],
      racePosition: input.read(1).unpack('C')[0],
      accel: input.read(1).unpack('C')[0],
      brake: input.read(1).unpack('C')[0],
      clutch: input.read(1).unpack('C')[0],
      handBrake: input.read(1).unpack('C')[0],
      gear: input.read(1).unpack('C')[0],
      steer: input.read(1).unpack('C')[0],
      normalizedDrivingLine: input.read(1).unpack('C')[0],
      normalizedAIBrakeDifference: input.read(1).unpack('C')[0],
      unknown2: input.read(1).unpack('C')[0]
    }

    bars = {}
    bars[:engineMaxRpm] = vals[:engineMaxRpm] / 50
    bars[:engineIdleRpm] = vals[:engineIdleRpm] / 50
    bars[:currentEngineRpm] = vals[:currentEngineRpm] / 50

    bars[:acceleration] = vals[:acceleration].collect{|v| v * 2 + 127}
    bars[:velocity] = vals[:velocity].collect{|v| v + 127}
    bars[:angularVelocity] = vals[:angularVelocity].collect{|v| v * 10 + 127}
    bars[:position] = vals[:position].collect{|v| v / 256 + 127}
    bars[:yaw] = vals[:yaw] * 10 + 127
    bars[:pitch] = vals[:pitch] * 10 + 127
    bars[:roll] = vals[:roll] * 10 + 127
    bars[:normalizedSuspensionTravel] = vals[:normalizedSuspensionTravel].collect{|v| v * 255}
    bars[:tireSlipRatio] = vals[:tireSlipRatio].collect{|v| v * 3 + 127}
    bars[:wheelRotationSpeed] = vals[:wheelRotationSpeed].collect{|v| v / 4 + 127}
    bars[:wheelOnRumbleStrip] = vals[:wheelOnRumbleStrip].collect{|v| v * 255}
    bars[:wheelInPuddleDepth] = vals[:wheelInPuddleDepth].collect{|v| v * 255}
    bars[:surfaceRumble] = vals[:surfaceRumble].collect{|v| v * 255}
    bars[:tireSlipAngle] = vals[:tireSlipAngle].collect{|v| v * 5 + 127}
    bars[:tireCombinedSlip] = vals[:tireCombinedSlip].collect{|v| v * 5 + 127}
    bars[:suspensionTravelMeters] = vals[:suspensionTravelMeters].collect{|v| v * 100 + 127}
    bars[:tireTemp] = vals[:tireTemp]

    bars[:speed] = vals[:speed]
    bars[:power] = vals[:power] / 32767 + 127
    bars[:torque] = vals[:torque] / 16 + 127
    bars[:boost] = vals[:boost] + 127
    bars[:fuel] = vals[:fuel]

    bars[:accel] = vals[:accel]
    bars[:brake] = vals[:brake]
    bars[:clutch] = vals[:clutch]
    bars[:handBrake] = vals[:handBrake]
    bars[:gear] = vals[:gear] * 20
    bars[:steer] = vals[:steer]

    bars.each_pair{|k,v|
      if bars[k].is_a?(Array)
        bars[k] = bars[k].collect{|v| v.to_i}
      else
        bars[k] = bars[k].to_i
      end
    }
    return {vals: vals, bars: bars}
  end
end

@telemetry = '{}'
Thread.start() do
  FH4TelemetryServer.new{|data| @telemetry = JSON.dump(data)}
end.abort_on_exception = true

sv = WEBrick::HTTPServer.new(Port: 3939, DocumentRoot: File.join(Dir.pwd, 'public'))
sv.mount_proc('/telemetry.json') do |req, res|
  res.content_type = 'application/json'
  res.body = @telemetry
end
sv.start
